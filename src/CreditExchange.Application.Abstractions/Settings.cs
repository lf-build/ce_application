using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Application
{
    public static class Settings
    {
        public static string ServiceName { get; } = "application";
        private static string Prefix { get; } = ServiceName.ToUpper();
        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");
        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");
        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        public static ServiceSettings Applicant { get; } = new ServiceSettings($"{Prefix}_APPLICANT", "applicant");
        public static ServiceSettings NumberGenerator { get; } = new ServiceSettings($"{Prefix}_NUMBERGENERATOR", "number-generator");
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "applications");
        public static ServiceSettings LookupService { get; } = new ServiceSettings($"{Prefix}_LOOKUPSERVICE", "lookup-service");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}