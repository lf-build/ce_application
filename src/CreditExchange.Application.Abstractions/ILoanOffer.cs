﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Application
{
    //TODO: Review later
    public interface ILoanOffer
    {        
        string OfferId { get; set; }
        double GivenLoanAmount { get; set; } 
        double DisbursedLoanAmount { get; set; }
        double AnnualPercentageRate { get; set; }
        double InterestRate { get; set; }
        LoanFrequency RepaymentFrequency { get; set; }
        double RepaymentInstallmentAmount { get; set; }
        double NumberOfIntallments { get; set; }
        bool IsSelectedOffer { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IOfferFee, OfferFee>))]
        IList<IOfferFee> OfferFees { get; set; }
    }
}
