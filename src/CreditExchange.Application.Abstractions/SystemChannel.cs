﻿namespace CreditExchange.Application {
    public enum SystemChannel {
        BorrowerPortal = 1,
        BackOfficePortal = 2,
        MerchantPortal = 3,
        AffiliatePortal = 4,
        Api = 5,
        Leadsquared = 6,
        NewBorrowerPortal = 7,
        App = 8
    }
}