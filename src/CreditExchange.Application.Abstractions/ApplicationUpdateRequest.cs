﻿using System;
using System.Collections.Generic;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Application {
    public class ApplicationUpdateRequest : IApplicationUpdateRequest {
        public double RequestedAmount { get; set; }
        public LoanFrequency RequestedTermType { get; set; }
        public double RequestedTermValue { get; set; }
        public string PurposeOfLoan { get; set; }
        public Source Source { get; set; }
        public string ApplicationNumber { get; set; }
        public LoanOffer InitialOffer { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<ILoanOffer, LoanOffer>))]
        public List<ILoanOffer> GivenOffers { get; set; }
        public LoanOffer SelectedOffer { get; set; }
        public double MonthlyRent { get; set; }
        public EmploymentDetail EmploymentDetail { get; set; }
        public string ResidenceType { get; set; }
        public SelfDeclareExpense SelfDeclareExpense { get; set; }
        public double Emi { get; set; }
        public double MonthlyExpenses { get; set; }
        public double CreditCardBalances { get; set; }
        public DateTimeOffset ApplicationDate { get; set; }
        public string CurrentAddressYear { get; set; }
        public string CurrentAddressMonth { get; set; }
        public string EmployementYear { get; set; }
        public string EmployementMonth { get; set; }
    }
}