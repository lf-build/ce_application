﻿
using CreditExchange.Applicant;

namespace CreditExchange.Application
{
    public class ApplicationIncomeModified
    {
        public IIncomeInformation NewIncomeInformation { get; set; }
        public IIncomeInformation OldIncomeInformation { get; set; }
        public string ApplicationNumber { get; set; }
    }
}
