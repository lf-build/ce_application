﻿using CreditExchange.Applicant;

namespace CreditExchange.Application
{
    public class ApplicationCreated
    {
        public IApplication Application { get; set; }
        public IApplicant Applicant { get; set; }
    }
}
