﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Application
{
    public class ApplicationLoanAmountModified
    {
        public double OldLoanAmount { get; set; }
        public double NewLoanAmount { get; set; }
        public string ApplicationNumber { get; set; }
    }
}
