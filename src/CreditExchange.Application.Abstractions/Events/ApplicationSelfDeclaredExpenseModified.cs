﻿namespace CreditExchange.Application
{
    public class ApplicationSelfDeclaredExpenseModified
    {
        public ISelfDeclareExpense NewSelfDeclareExpense { get; set; }

        public ISelfDeclareExpense OldSelfDeclareExpense { get; set; }
        public string ApplicationNumber { get; set; }

        public string ResidenceType { get; set; }
    }
}
