﻿

using CreditExchange.Applicant;

namespace CreditExchange.Application
{
    public class ApplicationEmailModified
    {
        public IEmailAddress NewEmailAddress { get; set; }

        public IEmailAddress OldEmailAddress { get; set; }
        public string ApplicationNumber { get; set; }
    }
}
