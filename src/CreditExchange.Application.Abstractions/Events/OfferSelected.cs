﻿namespace CreditExchange.Application
{
    public class OfferSelected
    {
        public ILoanOffer SelectedOffer { get; set; }
        public string ApplicationNumber { get; set; }
        
    }
}
