﻿

using CreditExchange.Applicant;

namespace CreditExchange.Application
{
    public class ApplicationEmploymentModified
    {
        public IEmploymentDetail NewEmploymentDetail { get; set; }
        public IEmploymentDetail OldEmploymentDetail { get; set; }
        public string ApplicationNumber { get; set; }
    }
}
