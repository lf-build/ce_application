﻿using CreditExchange.Applicant;

namespace CreditExchange.Application
{
    public class ApplicationModified
    {
        public IApplication Application { get; set; }
        public IApplicant Applicant { get; set; }
    }
}