using CreditExchange.Applicant;

namespace CreditExchange.Application
{
    public class UpdateScheduleFulfillment
    {
        public IApplication Application { get; set; }
        public IApplicant Applicant { get; set; }
    }
}