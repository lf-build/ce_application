﻿

using CreditExchange.Applicant;

namespace CreditExchange.Application
{
    public class ApplicationBankModified
    {
        public IBankInformation NewBankInformation { get; set; }
        public IBankInformation OldBankInformation { get; set; }
        public string ApplicationNumber { get; set; }
    }
}
