﻿using LendFoundry.Foundation.Date;
using System;

namespace CreditExchange.Application
{
    public interface IScheduleDetail
    {
        string PreferredAddress { get; set; }
        DateTimeOffset ScheduleDate { get; set; }
        string ScheduleTime { get; set; }
    }
}
