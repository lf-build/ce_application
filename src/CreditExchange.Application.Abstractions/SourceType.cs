﻿namespace CreditExchange.Application {
    public enum SourceType {
        Organic = 1,
        Merchant = 2,
        Affiliate = 3,
        Broker = 4,
        LeadAgg = 5,
        CRM = 6
    }
}