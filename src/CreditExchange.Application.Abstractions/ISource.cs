﻿namespace CreditExchange.Application
{
    public interface ISource
    {
        string TrackingCode { get; set; }

        string TrackingCodeMedium { get; set; }
        string TrackingCodeCampaign { get; set; }

        string TrackingCodeTerm { get; set; }
        string TrackingCodeContent { get; set; }
        SystemChannel SystemChannel { get; set; }

        SourceType SourceType { get; set; }

        string SourceReferenceId { get; set; }
        string GCLId { get; set; }
    }
}