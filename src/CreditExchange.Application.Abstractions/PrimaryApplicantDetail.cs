﻿using CreditExchange.Applicant;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CreditExchange.Application
{
    public class PrimaryApplicantDetail : IPrimaryApplicantDetail
    {
        public string Password { get; set; }
        public string UserName { get; set; }
        public string AadhaarNumber { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBankInformation, BankInformation>))]
        public IBankInformation BankInformation { get; set; }
        public DateTimeOffset DateOfBirth { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IEducationInformation, EducationInformation>))]
        public IEducationInformation EducationInformation { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        public IEmailAddress EmailAddress { get; set; }
        [JsonConverter(typeof(InterfaceConverter<Applicant.IEmploymentDetail, Applicant.EmploymentDetail>))]
        public Applicant.IEmploymentDetail EmploymentDetail { get; set; }
        public string FirstName { get; set; }
        public Gender Gender { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IIncomeInformation, IncomeInformation>))]
        public IIncomeInformation IncomeInformation { get; set; }
        public string LastName { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public string MiddleName { get; set; }
        public string PermanentAccountNumber { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        public List<IPhoneNumber> PhoneNumbers { get; set; }
        public string Salutation { get; set; }
        public string UserId { get; set; }
    }
}
