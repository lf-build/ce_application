﻿using System.Collections.Generic;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;

namespace CreditExchange.Application {
    public class Application : Aggregate, IApplication {
        public Application () { }

        public Application (IApplicationRequest applicationRequest) {
            RequestedAmount = applicationRequest.RequestedAmount;
            RequestedTermType = applicationRequest.RequestedTermType;
            RequestedTermValue = applicationRequest.RequestedTermValue;
            PurposeOfLoan = applicationRequest.PurposeOfLoan;
            Source = applicationRequest.Source;
            // EmploymentDetail = applicationRequest.EmploymentDetail;
            ResidenceType = applicationRequest.ResidenceType;
            SelfDeclareExpense = applicationRequest.SelfDeclareExpense;
            BankInformation = applicationRequest.PrimaryApplicant.BankInformation;
            EmailAddress = applicationRequest.PrimaryApplicant.EmailAddress;
            EmploymentDetail = applicationRequest.PrimaryApplicant.EmploymentDetail;
            IncomeInformation = applicationRequest.IncomeInformation;
            // if (applicationRequest.PrimaryApplicant.Addresses != null && applicationRequest.PrimaryApplicant.Addresses.Count > 0)
            Addresses = applicationRequest.PrimaryApplicant.Addresses;
            PhoneNumbers = applicationRequest.PrimaryApplicant.PhoneNumbers;
            OtherPurposeDescription = applicationRequest.OtherPurposeDescription;
            PromoCode = applicationRequest.PromoCode;
            TrackingCode = applicationRequest.TrackingCode;
            SodexoCode = applicationRequest.SodexoCode;
            if (applicationRequest.PreferredAddress != null)
                PreferredAddress = applicationRequest.PreferredAddress;
            if (applicationRequest.ScheduleDate != null)
                ScheduleDate = new TimeBucket (applicationRequest.ScheduleDate);
            if (applicationRequest.ScheduleTime != null)
                ScheduleTime = applicationRequest.ScheduleTime;
            LeadSquaredCRMId = applicationRequest.LeadSquaredCRMId;
            CurrentAddressYear = applicationRequest.CurrentAddressYear;
            CurrentAddressMonth = applicationRequest.CurrentAddressMonth;
            EmployementYear = applicationRequest.EmployementYear;
            EmployementMonth = applicationRequest.EmployementMonth;
        }

        public double RequestedAmount { get; set; }
        public LoanFrequency RequestedTermType { get; set; }
        public double RequestedTermValue { get; set; }
        public string PurposeOfLoan { get; set; }
        //TODO: What is other purpose of loan?
        public string OtherPurposeDescription { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ISource, Source>))]
        public ISource Source { get; set; }
        public string ApplicationNumber { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IEmploymentDetail, EmploymentDetail>))]
        public IEmploymentDetail EmploymentDetail { get; set; }
        public string ResidenceType { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ISelfDeclareExpense, SelfDeclareExpense>))]
        public ISelfDeclareExpense SelfDeclareExpense { get; set; }
        public TimeBucket ApplicationDate { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IBankInformation, BankInformation>))]
        public IBankInformation BankInformation { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IEmailAddress, EmailAddress>))]
        public IEmailAddress EmailAddress { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IIncomeInformation, IncomeInformation>))]
        public IIncomeInformation IncomeInformation { get; set; }
        public TimeBucket ExpiryDate { get; set; }
        public TimeBucket ExpiredDate { get; set; }
        public string ApplicantId { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        public List<IPhoneNumber> PhoneNumbers { get; set; }
        public string PromoCode { get; set; }
        public string TrackingCode { get; set; }

        public bool? HunterStatus { get; set; }
        public string SodexoCode { get; set; }
        public string HunterId { get; set; }
        public string SchemeCode { get; set; }
        public string DocumentCollectedSource { get; set; }
        public string PreferredAddress { get; set; }
        public TimeBucket ScheduleDate { get; set; }
        public string ScheduleTime { get; set; }
        public string LeadSquaredCRMId { get; set; }
        public string CurrentAddressYear { get; set; }
        public string CurrentAddressMonth { get; set; }
        public string EmployementYear { get; set; }
        public string EmployementMonth { get; set; }

    }
}