﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Application
{
    public class LoanOffer :ILoanOffer
    {      
        public string OfferId { get; set; }
        public double GivenLoanAmount { get; set; }
        public double DisbursedLoanAmount { get; set; }
        public double AnnualPercentageRate { get; set; }
        public double InterestRate { get; set; }
        public LoanFrequency RepaymentFrequency { get; set; }
        public double RepaymentInstallmentAmount { get; set; }
        public double NumberOfIntallments { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IOfferFee, OfferFee>))]
        public IList<IOfferFee> OfferFees { get; set; }
        public bool IsSelectedOffer { get; set; }
    }
}
