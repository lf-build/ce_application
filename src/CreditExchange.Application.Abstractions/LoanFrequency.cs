﻿
namespace CreditExchange.Application
{
    public enum LoanFrequency
    {
        Daily = 1,
        Weekly = 2,
        BiWeekly = 3,
        SemiMonthly = 4,
        Monthly = 5,
        Quarterly = 6,
        SixMonthly = 7,
        Yearly = 8,
        Custom = 100
    }
}
