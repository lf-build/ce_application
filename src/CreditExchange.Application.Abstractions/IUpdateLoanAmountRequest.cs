﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Application
{
    public interface IUpdateLoanAmountRequest
    {
        double RequestLoanAmount { get; set; }
    }
}
