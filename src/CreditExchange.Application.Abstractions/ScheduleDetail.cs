﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Application
{
    public class ScheduleDetail : IScheduleDetail
    {
        public string PreferredAddress { get; set; }
        public DateTimeOffset ScheduleDate { get; set; }
        public string ScheduleTime { get; set; }
    }
}
