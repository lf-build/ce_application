﻿
namespace CreditExchange.Application
{
    public interface ISelfDeclareExpenseRequest
    {
        double MonthlyRent { get; set; }
        double DebtPayments { get; set; }
        double MonthlyExpenses { get; set; }
        double CreditCardBalances { get; set; }
    }
}
