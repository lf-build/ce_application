﻿using System;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Application {
    public interface IApplicationRequest {
        double RequestedAmount { get; set; }
        LoanFrequency RequestedTermType { get; set; }
        double RequestedTermValue { get; set; }
        string PurposeOfLoan { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IIncomeInformation, IncomeInformation>))]
        IIncomeInformation IncomeInformation { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ISource, Source>))]
        ISource Source { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IEmploymentDetail, EmploymentDetail>))]
        IEmploymentDetail EmploymentDetail { get; set; }

        string ResidenceType { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ISelfDeclareExpense, SelfDeclareExpense>))]
        ISelfDeclareExpense SelfDeclareExpense { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IApplicantRequest, ApplicantRequest>))]
        IPrimaryApplicantDetail PrimaryApplicant { get; set; }

        string OtherPurposeDescription { get; set; }
        string PromoCode { get; set; }
        string TrackingCode { get; set; }
        string SodexoCode { get; set; }
        string HunterId { get; set; }
        string SchemeCode { get; set; }
        string DocumentCollectedSource { get; set; }
        string PreferredAddress { get; set; }
        DateTimeOffset ScheduleDate { get; set; }
        string ScheduleTime { get; set; }
        string LeadSquaredCRMId { get; set; }
        string CurrentAddressYear { get; set; }
        string CurrentAddressMonth { get; set; }
        string EmployementYear { get; set; }
        string EmployementMonth { get; set; }
    }
}