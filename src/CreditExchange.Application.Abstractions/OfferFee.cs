﻿namespace CreditExchange.Application
{
    public class OfferFee : IOfferFee
    {
        public double FeeAmount { get; set; }
        public OfferFeeType FeeType { get; set; }
        public double FeePercentage { get; set; }
        public bool IsIncludedInLoanAmount { get; set; }
    }
}
