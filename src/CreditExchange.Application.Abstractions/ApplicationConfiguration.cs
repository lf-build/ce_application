﻿namespace CreditExchange.Application
{
    public class ApplicationConfiguration : IApplicationConfiguration
    {
        public string InitialStatus { get; set; }
        public string InitialLeadStatus { get; set; }
        public int ExpiryDays { get; set; }
    }
}
