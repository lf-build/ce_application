﻿using System;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Application {
    public class ApplicationRequest : IApplicationRequest {
        [JsonConverter (typeof (InterfaceConverter<IPrimaryApplicantDetail, PrimaryApplicantDetail>))]
        public IPrimaryApplicantDetail PrimaryApplicant { get; set; }
        public double RequestedAmount { get; set; }
        public LoanFrequency RequestedTermType { get; set; }
        public double RequestedTermValue { get; set; }
        public string PurposeOfLoan { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ISource, Source>))]
        public ISource Source { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IIncomeInformation, IncomeInformation>))]
        public IIncomeInformation IncomeInformation { get; set; }
        public string ApplicationNumber { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IEmploymentDetail, EmploymentDetail>))]
        public IEmploymentDetail EmploymentDetail { get; set; }
        public string ResidenceType { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ISelfDeclareExpense, SelfDeclareExpense>))]
        public ISelfDeclareExpense SelfDeclareExpense { get; set; }
        public DateTimeOffset ApplicationDate { get; set; }
        public string OtherPurposeDescription { get; set; }
        public string PromoCode { get; set; }
        public string TrackingCode { get; set; }
        public string SodexoCode { get; set; }

        public string HunterId { get; set; }

        public string SchemeCode { get; set; }

        public string DocumentCollectedSource { get; set; }
        public string PreferredAddress { get; set; }
        public DateTimeOffset ScheduleDate { get; set; }
        public string ScheduleTime { get; set; }

        public string LeadSquaredCRMId { get; set; }
        public string CurrentAddressYear { get; set; }
        public string CurrentAddressMonth { get; set; }
        public string EmployementYear { get; set; }
        public string EmployementMonth { get; set; }
    }
}