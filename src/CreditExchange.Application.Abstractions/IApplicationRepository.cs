﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditExchange.Applicant;
using System;
using LendFoundry.Foundation.Date;

namespace CreditExchange.Application
{
    public interface IApplicationRepository : IRepository<IApplication>
    {
        Task<IApplication> GetByApplicationNumber(string applicationNumber);
        Task<IApplication> UpdateBankInformation(string applicationNumber, IBankInformation bankInformation);
        Task<IApplication> UpdateEmailInformation(string applicationNumber, IEmailAddress emailInformation);
        Task<IApplication> UpdateIncomeInformation(string applicationNumber, IIncomeInformation incomeInformation);
        Task<IApplication> UpdateSelfDeclaredExpense(string applicationNumber, ISelfDeclareExpense declareInformation);
        Task<IApplication> UpdateEmploymentInformation(string applicationNumber, IEmploymentDetail employmentInformation);
        Task<IApplication> UpdateApplication(string applicationNumber, IApplication applicationRequest);
        Task<IApplication> UpdateAddress(string applicationNumber, IAddress addressRequest);
        Task<string> UpdateResidenceType(string applicationNumber, string residenceType);
        Task UpdateLoanPurpose(string applicationNumber, string loanPurpose, string otherLoanPurpose);
        Task<List<string>> GetAllApplicationNumber();
        Task ExtendApplicationExpiry(string applicationNumber, int byDays, bool shouldExtendOnExpiryDate);
        Task<List<IApplication>> GetApplicationsWithTrackingCode(TimeBucket FromDate, TimeBucket ToDate);
        Task<bool> UpdateHunterStatus(string applicationNumber,string hunterid, bool? hunterstatus);
        Task<bool> UpdateSchemeCode(string applicationNumber, string schemecode);
        Task<bool> UpdateDocumentCollectedSource(string applicationNumber, string documentcollectedsource);
        Task<bool> UpdateScheduleFulfillment(string applicationNumber, TimeBucket scheduledate, string preferredaddress,string scheduleTime);

        Task UpdateLeadSquaredCRMId(string applicationNumber, string leadSquaredCRMId);
    }
}