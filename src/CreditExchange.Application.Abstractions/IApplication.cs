﻿using System.Collections.Generic;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;

namespace CreditExchange.Application {
    public interface IApplication : IAggregate {
        string ApplicationNumber { get; set; }
        string ApplicantId { get; set; }

        double RequestedAmount { get; set; }
        LoanFrequency RequestedTermType { get; set; }
        double RequestedTermValue { get; set; }

        string PurposeOfLoan { get; set; }

        string OtherPurposeDescription { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IAddress, Address>))]
        List<IAddress> Addresses { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        List<IPhoneNumber> PhoneNumbers { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ISource, Source>))]
        ISource Source { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IEmploymentDetail, EmploymentDetail>))]
        IEmploymentDetail EmploymentDetail { get; set; }

        string ResidenceType { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ISelfDeclareExpense, SelfDeclareExpense>))]
        ISelfDeclareExpense SelfDeclareExpense { get; set; }

        TimeBucket ApplicationDate { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IBankInformation, BankInformation>))]
        IBankInformation BankInformation { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IEmailAddress, EmailAddress>))]
        IEmailAddress EmailAddress { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IIncomeInformation, IncomeInformation>))]
        IIncomeInformation IncomeInformation { get; set; }
        TimeBucket ExpiryDate { get; set; }
        TimeBucket ExpiredDate { get; set; }
        string PromoCode { get; set; }
        string TrackingCode { get; set; }
        bool? HunterStatus { get; set; }
        string SodexoCode { get; set; }
        string HunterId { get; set; }
        string SchemeCode { get; set; }
        string DocumentCollectedSource { get; set; }
        string PreferredAddress { get; set; }
        TimeBucket ScheduleDate { get; set; }
        string ScheduleTime { get; set; }
        string LeadSquaredCRMId { get; set; }

        string CurrentAddressYear { get; set; }
        string CurrentAddressMonth { get; set; }
        string EmployementYear { get; set; }
        string EmployementMonth { get; set; }
    }
}