﻿
namespace CreditExchange.Application
{
    public class SelfDeclareExpense : ISelfDeclareExpense
    {
        public SelfDeclareExpense() { }
       
        public SelfDeclareExpense(ISelfDeclareExpenseRequest expenseRequest)
        {
            MonthlyRent = expenseRequest.MonthlyRent;
            DebtPayments = expenseRequest.DebtPayments;
            MonthlyExpenses = expenseRequest.MonthlyExpenses;
            CreditCardBalances = expenseRequest.CreditCardBalances;

        }
        public double MonthlyRent { get; set; }
        public double MonthlyExpenses { get; set; }
        public double CreditCardBalances { get; set; }
        public double DebtPayments { get; set; }
    }
}
