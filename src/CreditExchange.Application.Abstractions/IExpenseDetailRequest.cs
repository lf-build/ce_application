﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Application
{
    public interface IExpenseDetailRequest
    {
        double? CreditCardBalances { get; set; }
        double? DebtPayments { get; set; }
        double? MonthlyExpenses { get; set; }
        double? MonthlyRent { get; set; }

        string ResidenceType { get; set; }
    }
}
