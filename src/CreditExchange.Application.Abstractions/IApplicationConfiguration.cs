﻿
namespace CreditExchange.Application
{
    public interface IApplicationConfiguration
    {
        string InitialStatus { get; set; }
        string InitialLeadStatus { get; set; }
        int ExpiryDays { get; set; }
    }
}
