namespace CreditExchange.Application
{
    public interface IOfferFee
    {
        double FeeAmount { get; set; }
        OfferFeeType FeeType { get; set; }
        double FeePercentage { get; set; }
        bool IsIncludedInLoanAmount { get; set; }
    }
}