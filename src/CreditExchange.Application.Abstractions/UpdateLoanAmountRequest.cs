﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Application
{
    public class UpdateLoanAmountRequest : IUpdateLoanAmountRequest
    {
        public double RequestLoanAmount { get; set; }
    }
}
