﻿using CreditExchange.Applicant;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CreditExchange.Application
{
   public interface IPrimaryApplicantDetail
    {
        string Password { get; set; }
        string UserName { get; set; }
        string AadhaarNumber { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        List<IAddress> Addresses { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IBankInformation, BankInformation>))]
        IBankInformation BankInformation { get; set; }
        DateTimeOffset DateOfBirth { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IEducationInformation, EducationInformation>))]
        IEducationInformation EducationInformation { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IEmailAddress, EmailAddress>))]
        IEmailAddress EmailAddress { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IEmploymentDetail, EmploymentDetail>))]
        IEmploymentDetail EmploymentDetail { get; set; }
        string FirstName { get; set; }
        Gender Gender { get; set; }      
        string LastName { get; set; }
        MaritalStatus MaritalStatus { get; set; }
        string MiddleName { get; set; }
        string PermanentAccountNumber { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        List<IPhoneNumber> PhoneNumbers { get; set; }
        string Salutation { get; set; }
        string UserId { get; set; }
    }
}
