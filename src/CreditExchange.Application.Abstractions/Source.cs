﻿namespace CreditExchange.Application
{
    public class Source : ISource
    {
        public string TrackingCode { get; set; }

        public string TrackingCodeMedium { get; set; }
        public string TrackingCodeCampaign { get; set; }
        public string TrackingCodeTerm { get; set; }
        public string TrackingCodeContent { get; set; }
        public SystemChannel SystemChannel { get; set; }
        public SourceType SourceType { get; set; }
        public string SourceReferenceId { get; set; }
        public string GCLId { get; set; }
    }
}