﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Application
{
    
        public class ExpenseDetailRequest : IExpenseDetailRequest
        {
            public double? CreditCardBalances { get; set; }
            public double? DebtPayments { get; set; }
            public double? MonthlyExpenses { get; set; }
            public double? MonthlyRent { get; set; }

            public string ResidenceType { get; set; }
        }
    
}
