﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CreditExchange.Applicant;
using System;
using LendFoundry.Foundation.Date;

namespace CreditExchange.Application
{
    public interface IApplicationService
    {
        Task<IApplicationResponse> Add(IApplicationRequest applicationRequest);     
        Task<IApplication> GetByApplicationNumber(string applicationNumber);
        Task UpdateBankInformation(string applicationNumber, IBankInformation bankInformationRequest);
        Task UpdateEmailInformation(string applicationNumber, IEmailAddress emailInformationRequest);
        Task UpdateIncomeInformation(string applicationNumber, IIncomeInformation incomeInformationRequest);
        Task UpdateSelfDeclaredExpense(string applicationNumber, IExpenseDetailRequest declareExpenseRequest);
        Task UpdateEmploymentDetail(string applicationNumber, IEmploymentDetail employmentInformationRequest);
        Task<List<string>> GetAllApplicationNumber();
        Task<IApplication> UpdateApplication(string applicationNumber, IApplicationRequest request);

        Task UpdateAddress(string applicationNumber, IAddress address);

        Task UpdateLoanAmount(string applicationNumber, IUpdateLoanAmountRequest requestLoanAmount);
        Task UpdateLoanPurpose(string applicationNumber, string loanPurpose, string otherLoanPurpose);
        Task ExtendApplicationExpiry(string applicationNumber, int byDays, bool shouldExtendOnExpiryDate);
        Task<List<IApplication>> GetApplicationsWithTrackingCode();
        Task UpdateHunterStatus(string applicationNumber,string hunterid, bool? hunterstatus);
        Task UpdateSchemeCode(string applicationNumber, string schemecode);
        Task UpdateDocumentCollectedSource(string applicationNumber, string documentcollectedsource);
        Task UpdateScheduleFulfillment(string applicationNumber, IScheduleDetail scheduleDetail);
        Task UpdateLeadSquaredCRMId(string applicationNumber, string leadSquaredCRMId);
    }
}   
