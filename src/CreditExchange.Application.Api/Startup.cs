﻿using System.IO;
using CreditExchange.Applicant.Client;
using CreditExchange.Application.Persistence;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;

namespace CreditExchange.Application.Api {
    public class Startup {
        public Startup (IHostingEnvironment env) { }
        public void ConfigureServices (IServiceCollection services) {
            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("v1", new Info {
                    Version = "v1",
                        Title = "Application"
                });
                c.AddSecurityDefinition ("apiKey", new ApiKeyScheme () {
                    Type = "apiKey",
                        Name = "Authorization",
                        Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                        In = "header"
                });
                c.DescribeAllEnumsAsStrings ();
                c.IgnoreObsoleteProperties ();
                c.DescribeStringEnumsInCamelCase ();
                c.IgnoreObsoleteActions ();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine (basePath, "CreditExchange.Application.Api.xml");
                c.IncludeXmlComments (xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor> ();

            services.AddTenantTime ();
            services.AddTokenHandler ();
            services.AddHttpServiceLogging (Settings.ServiceName);
            services.AddConfigurationService<ApplicationConfiguration> (Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub (Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService (Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddNumberGeneratorService (Settings.NumberGenerator.Host, Settings.NumberGenerator.Port);
            services.AddApplicantService (Settings.Applicant.Host, Settings.Applicant.Port);
            services.AddSingleton<IMongoConfiguration> (p => new MongoConfiguration (Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddLookupService (Settings.LookupService.Host, Settings.LookupService.Port);

            services.AddTransient<ApplicationConfiguration> (p => p.GetService<IConfigurationService<ApplicationConfiguration>> ().Get ());
            services.AddTransient<IApplicationRepository, ApplicationRepository> ();
            services.AddTransient<IApplicationService, ApplicationService> ();
            services.AddTransient<IApplicationConfiguration, ApplicationConfiguration> ();

            services.AddMvc ().AddLendFoundryJsonOptions ();
            services.AddCors ();
        }

        public void Configure (IApplicationBuilder app, IHostingEnvironment env) {

            app.UseSwagger ();

            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "MobileVerification Service");
            });
            app.UseCors (env);
            app.UseErrorHandling ();
            app.UseRequestLogging ();
            //app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseMvc ();

        }
    }

}