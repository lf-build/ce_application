﻿﻿using CreditExchange.Applicant;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;

namespace CreditExchange.Application.Api.Controllers
{
    [Route("/")]
    public class ApplicationController : ExtendedController
    {
        #region Constructors
        public ApplicationController(IApplicationService service)
        {
            ApplicationService = service;
        }
        #endregion

        #region Private Properties
        private static NoContentResult NoContentResult { get; } = new NoContentResult();
        private IApplicationService ApplicationService { get; }

        #endregion

        #region Public Methods
        [HttpPost]
        [ProducesResponseType(typeof(IApplicationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Add([FromBody]ApplicationRequest applicationRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ApplicationService.Add(applicationRequest));
                }
                catch (UnableToCreateUserException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
                catch (AggregateException exception)
                {
                    return new ErrorResult(409, exception.InnerException?.Message);
                }
            });
        }

        //[HttpPost("lead")]
        //public async Task<IActionResult> AddLead([FromBody]ApplicationRequest applicationRequest)
        //{
        //    return await ExecuteAsync(async () =>
        //    {
        //        try
        //        {
        //            return Ok(await ApplicationService.AddLead(applicationRequest));
        //        }
        //        catch (UnableToCreateUserException exception)
        //        {
        //            return ErrorResult.BadRequest(exception.Message);
        //        }
        //        catch (AggregateException exception)
        //        { 
        //            return new ErrorResult(409, exception.InnerException?.Message);
        //        }
        //        catch (Exception exception)
        //        { 
        //            return new ErrorResult(409, exception.InnerException?.Message);
        //        }
        //    });
        //}

        [HttpGet("{applicationNumber}")]
        [ProducesResponseType(typeof(IApplication), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Get(string applicationNumber) =>
            await ExecuteAsync(async () => Ok(await ApplicationService.GetByApplicationNumber(applicationNumber)));

        //[HttpPost("{applicationId}/loanapplication")]
        //public IActionResult UpdateApplication(string applicationId, [FromBody]ApplicationUpdateRequest applicationRequest) =>
        //   Execute(() => { ApplicationService.UpdateApplication(applicationId, applicationRequest); return NoContentResult; });

        [HttpPut("{applicationNumber}/bankinformation")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> LinkBankInformation(string applicationNumber, [FromBody]BankInformation bankInformationRequest) =>
            await ExecuteAsync(async () => { await ApplicationService.UpdateBankInformation(applicationNumber, bankInformationRequest); return NoContentResult; });

        [HttpPut("{applicationNumber}/emailinforamtion")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> LinkEmailInformation(string applicationNumber, [FromBody]EmailAddress emailInfoRequest) =>
            await ExecuteAsync(async () => { await ApplicationService.UpdateEmailInformation(applicationNumber, emailInfoRequest); return NoContentResult; });

        [HttpPut("{applicationNumber}/incomeinformation")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> LinkIncomeInformation(string applicationNumber, [FromBody]IncomeInformation incomeInfoRequest) =>
             await ExecuteAsync(async () => { await ApplicationService.UpdateIncomeInformation(applicationNumber, incomeInfoRequest); return NoContentResult; });

        [HttpPut("{applicationNumber}/declaredexpense")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> LinkSelfDeclaredExpense(string applicationNumber, [FromBody]ExpenseDetailRequest declareExpenseRequest) =>
            await ExecuteAsync(async () => { await ApplicationService.UpdateSelfDeclaredExpense(applicationNumber, declareExpenseRequest); return NoContentResult; });

        [HttpPut("{applicationNumber}/employmentinformation")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> LinkEmploymentInformation(string applicationNumber, [FromBody]EmploymentDetail employmentDetailRequest) =>
           await ExecuteAsync(async () => { await ApplicationService.UpdateEmploymentDetail(applicationNumber, employmentDetailRequest); return NoContentResult; });

        [HttpPut("{applicationNumber}")]
        [ProducesResponseType(typeof(IApplication), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateApplication(string applicationNumber, [FromBody]ApplicationRequest applicationRequest)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.UpdateApplication(applicationNumber, applicationRequest)));
        }

        [HttpPut("{applicationNumber}/address")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateAddress(string applicationNumber, [FromBody]Address address) =>
            await ExecuteAsync(async () => { await ApplicationService.UpdateAddress(applicationNumber, address); return NoContentResult; });


        [HttpPut("{applicationNumber}/requestLoanAmount")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateLoanAmount(string applicationNumber, [FromBody]UpdateLoanAmountRequest requestLoanAmount) =>
           await ExecuteAsync(async () => { await ApplicationService.UpdateLoanAmount(applicationNumber, requestLoanAmount); return NoContentResult; });

        [HttpPut("requestLoanPurpose/{applicationNumber}/{loanpurpose}/{otherloanpurpose?}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateLoanPurpose(string applicationNumber, string loanPurpose, string otherLoanPurpose = null) =>
           await ExecuteAsync(async () => { await ApplicationService.UpdateLoanPurpose(applicationNumber, loanPurpose, otherLoanPurpose); return NoContentResult; });

        [HttpPut("ExtendApplicationExpiry/{applicationNumber}/{byDays}/{shouldExtendExpiryDate}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ExtendApplicationExpiry(string applicationNumber, int byDays, bool shouldExtendExpiryDate) =>
            await ExecuteAsync(async () => { await ApplicationService.ExtendApplicationExpiry(applicationNumber, byDays, shouldExtendExpiryDate); return NoContentResult; });

        [HttpGet("/alltrackingcode")]
        [ProducesResponseType(typeof(IApplication), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetApplicationsWithTrackingCode()
        {
            try
            {
                var results = await ApplicationService.GetApplicationsWithTrackingCode();
                return Ok(results);
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }
        [HttpPut("{applicationNumber}/hunterstatus/{hunterid}/{hunterstatus}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateHunterStatus(string applicationNumber, string hunterid, bool? hunterstatus)=>      
            await ExecuteAsync(async () => { await ApplicationService.UpdateHunterStatus(applicationNumber, hunterid, hunterstatus); return NoContentResult; });

        [HttpPut("schemecode/{applicationNumber}/{schemecode}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateSchemeCode(string applicationNumber, string schemecode) =>
           await ExecuteAsync(async () => { await ApplicationService.UpdateSchemeCode(applicationNumber,schemecode); return NoContentResult; });

        [HttpPut("documentcollectedsource/{applicationNumber}/{documentcollectedsource}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateDocumentCollectedSource(string applicationNumber, string documentcollectedsource) =>
           await ExecuteAsync(async () => { await ApplicationService.UpdateDocumentCollectedSource(applicationNumber, documentcollectedsource); return NoContentResult; });

        [HttpPut("scheduledetail/{applicationNumber}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateScheduleFulfillment(string applicationNumber, [FromBody]ScheduleDetail scheduleDetail) =>
            await ExecuteAsync(async () => { await ApplicationService.UpdateScheduleFulfillment(applicationNumber, scheduleDetail); return NoContentResult; });
        [HttpPut("leadSquaredCRMId/{applicationNumber}/{leadSquaredCRMId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateLeadSquaredCRMId(string applicationNumber,string leadSquaredCRMId) =>
           await ExecuteAsync(async () => { await ApplicationService.UpdateLeadSquaredCRMId(applicationNumber, leadSquaredCRMId); return NoContentResult; });
        #endregion
    }
}