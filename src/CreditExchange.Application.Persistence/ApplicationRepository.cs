﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditExchange.Applicant;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace CreditExchange.Application.Persistence {
    public class ApplicationRepository : MongoRepository<IApplication, Application>, IApplicationRepository {
        static ApplicationRepository () {
            BsonSerializer.RegisterSerializer (new ImpliedImplementationInterfaceSerializer<IApplicant, IApplicant> ());
            BsonClassMap.RegisterClassMap<Application> (map => {
                map.AutoMap ();
                //map.MapProperty(p => p).SetIgnoreIfDefault(true);
                // map.MapProperty(p => p.PurposeOfLoan).SetSerializer(new EnumSerializer<PurposeOfLoan>(BsonType.String));
                // map.MapProperty(p => p.ResidenceType).SetSerializer(new EnumSerializer<ResidenceType>(BsonType.String));
                var type = typeof (Application);

                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass (true);
            });
            BsonClassMap.RegisterClassMap<TimeBucket> (map => {
                map.AutoMap ();
                map.MapMember (m => m.Time).SetSerializer (new DateTimeOffsetSerializer (BsonType.Document));

                var type = typeof (TimeBucket);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass (false);
            });

            BsonClassMap.RegisterClassMap<Applicant.Applicant> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.MaritalStatus).SetSerializer (new EnumSerializer<MaritalStatus> (BsonType.String));
                map.MapProperty (p => p.Addresses).SetIgnoreIfDefault (true);
                map.MapProperty (p => p.PhoneNumbers).SetIgnoreIfDefault (true);
                var type = typeof (Applicant.Applicant);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<Address> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.AddressType).SetSerializer (new EnumSerializer<AddressType> (BsonType.String));
                var type = typeof (Address);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<PhoneNumber> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.PhoneType).SetSerializer (new EnumSerializer<PhoneType> (BsonType.String));
                var type = typeof (PhoneNumber);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<EmailAddress> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.EmailType).SetSerializer (new EnumSerializer<EmailType> (BsonType.String));
                var type = typeof (EmailAddress);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<Source> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.SystemChannel).SetSerializer (new EnumSerializer<SystemChannel> (BsonType.String));
                map.MapProperty (p => p.SourceType).SetSerializer (new EnumSerializer<SourceType> (BsonType.String));
                var type = typeof (Source);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LoanOffer> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.RepaymentFrequency).SetSerializer (new EnumSerializer<LoanFrequency> (BsonType.String));
                var type = typeof (LoanOffer);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<EmploymentDetail> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.EmploymentStatus).SetSerializer (new EnumSerializer<EmploymentStatus> (BsonType.String));
                var type = typeof (EmploymentDetail);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<IncomeInformation> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.PaymentFrequency).SetSerializer (new EnumSerializer<Applicant.PaymentFrequency> (BsonType.String));
                var type = typeof (IncomeInformation);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<SelfDeclareExpense> (map => {
                map.AutoMap ();
                var type = typeof (SelfDeclareExpense);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<BankInformation> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.AccountType).SetSerializer (new EnumSerializer<AccountType> (BsonType.String));
                var type = typeof (BankInformation);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<OfferFee> (map => {
                map.AutoMap ();
                map.MapProperty (p => p.FeeType).SetSerializer (new EnumSerializer<OfferFeeType> (BsonType.String));
                var type = typeof (OfferFee);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public ApplicationRepository (LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration) : base (tenantService, configuration, "application") {
            CreateIndexIfNotExists ("applications_applicationId", Builders<IApplication>.IndexKeys.Ascending (i => i.TenantId).Ascending (i => i.ApplicationNumber));
        }

        public async Task<IApplication> GetByApplicationNumber (string applicationNumber) {
            var filter = Builders<IApplication>.Filter.Where (application => application.TenantId == TenantService.Current.Id && application.ApplicationNumber == applicationNumber);
            return await Collection.Find (filter).FirstOrDefaultAsync ();
        }

        public async Task<string> GetApplicantId (string applicationNumber) {

            var application = await Collection.Find (GetByApplicationNumberFilter (applicationNumber)).FirstOrDefaultAsync ();
            return application.ApplicantId;
        }

        public async Task<IApplication> UpdateBankInformation (string applicationNumber, IBankInformation bankInformation) {
            var application = await GetByApplicationNumber (applicationNumber);
            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} not found");

            if (!string.IsNullOrWhiteSpace (bankInformation.BankId)) {
                if (application.BankInformation == null || !application.BankInformation.BankId.Equals (bankInformation.BankId)) {
                    throw new NotFoundException ($"BankId {bankInformation.BankId} is not found for Application {applicationNumber}");
                }
            } else {
                bankInformation.BankId = Guid.NewGuid ().ToString ("N");
            }

            application.BankInformation = bankInformation;
            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.BankInformation, bankInformation));

            return application;
        }

        public async Task<IApplication> UpdateEmailInformation (string applicationNumber, IEmailAddress emailInformation) {
            var application = await GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} not found");

            if (!string.IsNullOrWhiteSpace (emailInformation.Id)) {
                if (application.EmailAddress == null || !application.EmailAddress.Id.Equals (emailInformation.Id)) {
                    throw new NotFoundException ($"Email address with id {emailInformation.Id} is not found for Application {applicationNumber}");
                }
            } else {
                emailInformation.Id = Guid.NewGuid ().ToString ("N");
            }

            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.EmailAddress, emailInformation));
            application.EmailAddress = emailInformation;
            return application;
        }

        public async Task<IApplication> UpdateIncomeInformation (string applicationNumber, IIncomeInformation incomeInformation) {
            var application = await GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} not found");

            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.IncomeInformation, incomeInformation));
            return application;
        }

        public async Task<IApplication> UpdateEmploymentInformation (string applicationNumber, IEmploymentDetail employmentInformation) {
            var application = await GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} not found");

            if (!string.IsNullOrWhiteSpace (employmentInformation.EmploymentId)) {
                if (application.EmploymentDetail == null || !application.EmploymentDetail.EmploymentId.Equals (employmentInformation.EmploymentId)) {
                    throw new NotFoundException ($"EmploymentId {employmentInformation.EmploymentId} is not found for Application {applicationNumber}");
                }
            } else {
                employmentInformation.EmploymentId = Guid.NewGuid ().ToString ("N");
            }
            application.EmploymentDetail = employmentInformation;
            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.EmploymentDetail, employmentInformation));

            return application;
        }

        public async Task UpdateApplication (string applicationNumber, IApplicationUpdateRequest applicationRequest) {
            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.RequestedAmount, applicationRequest.RequestedAmount)
                .Set (a => a.RequestedTermType, applicationRequest.RequestedTermType)
                .Set (a => a.RequestedTermValue, applicationRequest.RequestedTermValue)
                .Set (a => a.PurposeOfLoan, applicationRequest.PurposeOfLoan)
                .Set (a => a.EmploymentDetail, applicationRequest.EmploymentDetail)
                .Set (a => a.ResidenceType, applicationRequest.ResidenceType)
                .Set (a => a.CurrentAddressYear, applicationRequest.CurrentAddressYear)
                .Set (a => a.CurrentAddressMonth, applicationRequest.CurrentAddressMonth)
                .Set (a => a.EmployementYear, applicationRequest.EmployementYear)
                .Set (a => a.EmployementMonth, applicationRequest.EmployementMonth)
                .Set (a => a.SelfDeclareExpense, applicationRequest.SelfDeclareExpense));
        }
        public async Task<IApplication> UpdateApplication (string applicationNumber, IApplication applicationRequest) {
            var application = await GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} not found");
            applicationRequest.TenantId = application.TenantId;
            applicationRequest.Id = application.Id;
            applicationRequest.ApplicantId = application.ApplicantId;

            await Collection.ReplaceOneAsync (GetByApplicationNumberFilter (applicationNumber), applicationRequest);
            return applicationRequest;
        }
        public async Task<IApplication> UpdateSelfDeclaredExpense (string applicationNumber, ISelfDeclareExpense declareExpense) {
            var application = await GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} not found");

            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.SelfDeclareExpense, declareExpense));

            return application;
        }

        public async Task<IApplication> UpdateAddress (string applicationNumber, IAddress addressRequest) {
            var application = await GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} not found");

            var oldAddress = application.Addresses.Where (x => x.AddressType.Equals (addressRequest.AddressType)).FirstOrDefault ();

            if (!string.IsNullOrWhiteSpace (addressRequest.AddressId)) {
                if (application.Addresses == null) {
                    throw new NotFoundException ($"Address is not found for Application {applicationNumber}");
                } else {
                    if (oldAddress == null || !oldAddress.AddressId.Equals (addressRequest.AddressId)) {
                        throw new NotFoundException ($"Address id {addressRequest.AddressId} is not found for Application {applicationNumber}");
                    }
                }
            } else {
                addressRequest.AddressId = Guid.NewGuid ().ToString ("N");
            }

            var addresses = application.Addresses;
            if (addressRequest.IsDefault)
                addresses.ForEach (a => { a.IsDefault = false; });

            addresses = addresses.Select (adr => adr.AddressId == addressRequest.AddressId ? addressRequest : adr).ToList ();

            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.Addresses, addresses));
            application.Addresses = addresses;
            return application;
        }
        public async Task<string> UpdateResidenceType (string applicationNumber, string residenceType) {

            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.ResidenceType, residenceType));

            return residenceType;
        }

        public async Task<List<string>> GetAllApplicationNumber () {
            return await Task.Run (() => Query.Select (app => app.ApplicationNumber).ToList ());
        }

        public async Task UpdateLoanPurpose (string applicationNumber, string loanPurpose, string otherLoanPurpose) {
            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.PurposeOfLoan, loanPurpose).Set (a => a.OtherPurposeDescription, otherLoanPurpose));
        }

        public async Task ExtendApplicationExpiry (string applicationNumber, int byDays, bool shouldExtendOnExpiryDate) {
            var application = await GetByApplicationNumber (applicationNumber);
            var extendedDate = shouldExtendOnExpiryDate == true ? new TimeBucket (application.ExpiryDate.Time.AddDays (byDays)) :
                new TimeBucket (DateTime.Now.AddDays (byDays));
            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.ExpiryDate, extendedDate));
        }
        private FilterDefinition<IApplication> GetByApplicationNumberFilter (string applicationNumber) {
            return Builders<IApplication>.Filter.Where (a => a.TenantId == TenantService.Current.Id &&
                a.ApplicationNumber == applicationNumber);
        }

        public async Task<List<IApplication>> GetApplicationsWithTrackingCode (TimeBucket FromDate, TimeBucket ToDate) {
            var filterswithDate = new List<FilterDefinition<IApplication>> ();

            filterswithDate.Add (Builders<IApplication>.Filter.Gte ("ApplicationDate.Time.DateTime", FromDate.Time.DateTime));
            filterswithDate.Add (Builders<IApplication>.Filter.Lte ("ApplicationDate.Time.DateTime", ToDate.Time.DateTime));
            filterswithDate.Add (Builders<IApplication>.Filter.Ne ("Source.TrackingCode", "BorrowerPortal"));
            filterswithDate.Add (Builders<IApplication>.Filter.Ne ("Source.TrackingCode", BsonNull.Value));

            var Applicationlist = Builders<IApplication>.Filter.And (filterswithDate);
            var ListofAppTrackingCode = await Collection.Find (Applicationlist).ToListAsync ();
            return ListofAppTrackingCode;

        }
        public async Task<bool> UpdateHunterStatus (string applicationNumber, string hunterid, bool? hunterstatus) {
            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.HunterStatus, hunterstatus).Set (a => a.HunterId, hunterid));
            return true;
        }
        public async Task<bool> UpdateSchemeCode (string applicationNumber, string schemecode) {
            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.SchemeCode, schemecode));
            return true;
        }
        public async Task<bool> UpdateDocumentCollectedSource (string applicationNumber, string documentcollectedsource) {
            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.DocumentCollectedSource, documentcollectedsource));
            return true;
        }
        public async Task<bool> UpdateScheduleFulfillment (string applicationNumber, TimeBucket scheduledate, string preferredaddress, string scheduletime) {
            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.ScheduleDate, scheduledate).Set (a => a.PreferredAddress, preferredaddress).Set (a => a.ScheduleTime, scheduletime));
            return true;
        }
        public async Task UpdateLeadSquaredCRMId (string applicationNumber, string leadSquaredCRMId) {
            await Collection.UpdateOneAsync (GetByApplicationNumberFilter (applicationNumber),
                Builders<IApplication>.Update.Set (a => a.LeadSquaredCRMId, leadSquaredCRMId));

        }
    }
}