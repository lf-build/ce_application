﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;


namespace CreditExchange.Application.Client
{
    public static class ApplicationServiceClientExtensions
    {
        public static IServiceCollection AddApplicationService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IApplicationServiceClientFactory>(p => new ApplicationServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicationServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
