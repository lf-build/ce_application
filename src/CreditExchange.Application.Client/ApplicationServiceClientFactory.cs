﻿﻿using System;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;

namespace CreditExchange.Application.Client
{
    public class ApplicationServiceClientFactory : IApplicationServiceClientFactory
    {
        #region Constructors
        public ApplicationServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        #endregion

        #region Private Properties
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        #endregion

        #region Public Methods
        public IApplicationService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ApplicationService(client);
        }
        #endregion
    }
}
