﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.Application.Client
{
    public interface IApplicationServiceClientFactory
    {
        IApplicationService Create(ITokenReader reader);
    }
}
