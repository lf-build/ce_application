﻿using LendFoundry.Foundation.Client;

using RestSharp;
using System.Threading.Tasks;
using System.Collections.Generic;
using CreditExchange.Applicant;
using System;

namespace CreditExchange.Application.Client
{
    public class ApplicationService : IApplicationService
    {
        #region Constructors
        public ApplicationService(IServiceClient client)
        {
            Client = client;
        }
        #endregion

        #region Private Properties
        private IServiceClient Client { get; }
        #endregion

        #region Public Methods
        public async Task<IApplication> GetByApplicationNumber(string applicationNumber)
        {
            var request = new RestRequest("/{applicationNumber}", Method.GET);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            return await Client.ExecuteAsync<Application>(request);
        }

        public async Task<IApplicationResponse> Add(IApplicationRequest applicationRequest)
        {
            var request = new RestRequest("/", Method.POST);
            request.AddJsonBody(applicationRequest);
            return await Client.ExecuteAsync<ApplicationResponse>(request);
        }

        public async Task<IApplicationResponse> AddLead(IApplicationRequest applicationRequest)
        {
            var request = new RestRequest("/lead", Method.POST);
            request.AddJsonBody(applicationRequest);
            return await Client.ExecuteAsync<ApplicationResponse>(request);
        }

        public async Task UpdateBankInformation(string applicationNumber, IBankInformation bankInformationRequest)
        {
            var request = new RestRequest("/{applicationNumber}/bankinformation", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddJsonBody(bankInformationRequest);
            await Client.ExecuteAsync(request);
        }

        public async Task UpdateEmailInformation(string applicationNumber, IEmailAddress emailInformationRequest)
        {
            var request = new RestRequest("/{applicationNumber}/emailinforamtion", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddJsonBody(emailInformationRequest);
            await Client.ExecuteAsync(request);
        }
        public async Task UpdateIncomeInformation(string applicationNumber, IIncomeInformation incomeInformationRequest)
        {
            var request = new RestRequest("/{applicationNumber}/incomeinformation", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddJsonBody(incomeInformationRequest);
            await Client.ExecuteAsync(request);
        }

        public async Task UpdateSelfDeclaredExpense(string applicationNumber, IExpenseDetailRequest declareExpenseRequest)
        {
            var request = new RestRequest("/{applicationNumber}/declaredexpense", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddJsonBody(declareExpenseRequest);
            await Client.ExecuteAsync(request);
        }

        public async Task UpdateEmploymentDetail(string applicationNumber, IEmploymentDetail employmentInformationRequest)
        {
            var request = new RestRequest("/{applicationNumber}/employmentinformation", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddJsonBody(employmentInformationRequest);
            await Client.ExecuteAsync(request);
        }

        public async Task<IApplication> UpdateApplication(string applicationNumber, IApplicationRequest UpdateRequest)
        {
            var request = new RestRequest("/{applicationNumber}", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddJsonBody(UpdateRequest);
            return await Client.ExecuteAsync<Application>(request);
        }

        public Task<List<string>> GetAllApplicationNumber()
        {
            throw new NotImplementedException();
        }

        public async Task UpdateAddress(string applicationNumber, IAddress address)
        {
            var request = new RestRequest("{applicationNumber}/address", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddJsonBody(address);
            await Client.ExecuteAsync(request);
        }

        public async Task UpdateLoanAmount(string applicationNumber, IUpdateLoanAmountRequest requestLoanAmount)
        {
            var request = new RestRequest("{applicationNumber}/requestLoanAmount", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddJsonBody(requestLoanAmount);
            await Client.ExecuteAsync(request);
        }

        public async Task UpdateLoanPurpose(string applicationNumber, string loanPurpose, string otherLoanPurpose)
        {
            var request = new RestRequest("requestLoanPurpose/{applicationNumber}/{loanpurpose}", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddUrlSegment("loanpurpose", loanPurpose);
            if (!string.IsNullOrEmpty(otherLoanPurpose))
            {
                request.Resource += "/{otherloanpurpose}";
                request.AddUrlSegment("otherloanpurpose", otherLoanPurpose);
            }
            await Client.ExecuteAsync(request);
        }

        public async Task ExtendApplicationExpiry(string applicationNumber, int byDays, bool shouldExtendOnExpiryDate)
        {
            var request = new RestRequest("ExtendApplicationExpiry/{applicationNumber}/{byDays}/{shouldExtendExpiryDate}", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddUrlSegment("byDays", byDays.ToString());
            request.AddUrlSegment("shouldExtendExpiryDate", shouldExtendOnExpiryDate.ToString());
            await Client.ExecuteAsync(request);
        }

        public async Task<List<IApplication>> GetApplicationsWithTrackingCode()
        { 
            var request = new RestRequest("/alltrackingcode", Method.GET);
            var result = await Client.ExecuteAsync<List<Application>>(request);
            return new List<IApplication>(result);
        }

        public async Task UpdateHunterStatus(string applicationNumber, string hunterid, bool? hunterstatus)
        {
            var request = new RestRequest("{applicationNumber}/hunterstatus/{hunterstatus}", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddUrlSegment("hunterid", hunterid);
            request.AddUrlSegment("hunterstatus",Convert.ToString(hunterstatus));
            await Client.ExecuteAsync(request);
        }

        public async Task UpdateSchemeCode(string applicationNumber, string schemecode)
        {
            var request = new RestRequest("schemecode/{applicationNumber}/{schemecode}", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddUrlSegment("schemecode", Convert.ToString(schemecode));
            await Client.ExecuteAsync(request);
        }

        public async Task UpdateDocumentCollectedSource(string applicationNumber, string documentcollectedsource)
        {
            var request = new RestRequest("documentcollectedsource/{applicationNumber}/{documentcollectedsource}", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddUrlSegment("documentcollectedsource", Convert.ToString(documentcollectedsource));
            await Client.ExecuteAsync(request);
        }
        public async Task UpdateScheduleFulfillment(string applicationNumber, IScheduleDetail scheduleDetail)
        {
            var request = new RestRequest("scheduledetail/{applicationNumber}", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddJsonBody(scheduleDetail);
            await Client.ExecuteAsync(request);
        }

        public async Task UpdateLeadSquaredCRMId(string applicationNumber, string leadSquaredCRMId)
        {
            var request = new RestRequest("leadSquaredCRMId/{applicationNumber}/{leadSquaredCRMId}", Method.PUT);
            request.AddUrlSegment("applicationNumber", applicationNumber);
            request.AddUrlSegment("applicationNumber", leadSquaredCRMId);
            await Client.ExecuteAsync(request);
        }
        #endregion
    }
}