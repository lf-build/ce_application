﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using CreditExchange.Applicant;
using CreditExchange.Applicant;
using LendFoundry.EventHub;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator;
using LendFoundry.NumberGenerator;
using Newtonsoft.Json;

namespace CreditExchange.Application {
    public class ApplicationService : IApplicationService {
        #region Constructor

        public ApplicationService
            (
                IApplicantService applicantService,
                IApplicationRepository applicationRepository,
                IGeneratorService applicationNumberGenerator,
                ILogger logger,
                IEventHubClient eventHubClient,
                ApplicationConfiguration applicationConfiguration,
                ITenantTime tenantTime,
                ILookupService lookupService
            ) {
                ApplicantService = applicantService;
                ApplicationRepository = applicationRepository;
                ApplicationNumberGenerator = applicationNumberGenerator;
                EventHubClient = eventHubClient;
                CommandExecutor = new CommandExecutor (logger);
                if (applicationConfiguration == null)
                    throw new ArgumentNullException (nameof (applicationConfiguration));
                ApplicationConfigurations = applicationConfiguration;
                TenantTime = tenantTime;
                Logger = logger;
                LookupService = lookupService;
            }

        #endregion

        #region Private Properties

        private IApplicantService ApplicantService { get; }
        private IApplicationRepository ApplicationRepository { get; }
        private IGeneratorService ApplicationNumberGenerator { get; }
        private IEventHubClient EventHubClient { get; }
        private ITenantTime TenantTime { get; }
        private CommandExecutor CommandExecutor { get; }
        private ApplicationConfiguration ApplicationConfigurations { get; }

        private ILookupService LookupService { get; }
        private ILogger Logger { get; }

        #endregion

        #region Public Methods

        public async Task<IApplicationResponse> Add (IApplicationRequest request) {
            var application = await SaveApplication (request);
            return new ApplicationResponse (application);
        }
        public async Task<IApplication> UpdateApplication (string applicationNumber, IApplicationRequest request) {
            Logger.Info ($"[ApplicationService] UpdateApplication method execution started for [{applicationNumber}] request : [{  (request != null ? JsonConvert.SerializeObject(request) : null)}] ");
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new Exception ("Application number is mandantory");

            Application application = null;
            IApplicant applicant = null;
            await Task.Run (() => {
                //EnsureInputIsValid(request);
                application = new Application (request);
                var currentApplication = ApplicationRepository.GetByApplicationNumber (applicationNumber).Result;
                var updateApplicantCommand = new Command (
                    () => {
                        var primaryApplicant = GetApplicatRequest (request.PrimaryApplicant);
                        var applicantRequest = new Applicant.Applicant (primaryApplicant);
                        applicantRequest.Id = currentApplication.ApplicantId;
                        Logger.Info ($"[ApplicationService] ApplicantService UpdateApplicant method execution started for [{applicationNumber}] ApplicantId : [{ currentApplication.ApplicantId}] Request : applicantRequest : [{  (applicantRequest != null ? JsonConvert.SerializeObject(applicantRequest) : null)}] ");
                        applicant = ApplicantService.UpdateApplicant (currentApplication.ApplicantId, applicantRequest).Result;
                        Logger.Info ($"[ApplicationService] ApplicantService UpdateApplication method execution finished for [{applicationNumber}] result : [{  (applicant != null ? JsonConvert.SerializeObject(applicant) : null)}] ");
                        //application.ApplicantId = applicant.Id;
                        if (applicant.BankInformation != null && applicant.BankInformation.Count > 0)
                            application.BankInformation.BankId = applicant.BankInformation[0].BankId;
                        if (applicant.EmailAddresses != null && applicant.EmailAddresses.Count > 0)
                            application.EmailAddress.Id = applicant.EmailAddresses[0].Id;
                        if (applicant.EmploymentDetails != null && applicant.EmploymentDetails.Count > 0)
                            application.EmploymentDetail.EmploymentId = applicant.EmploymentDetails[0].EmploymentId;
                        if (applicant.PhoneNumbers != null && applicant.PhoneNumbers.Count > 0)
                            application.PhoneNumbers = applicant.PhoneNumbers;
                        if (applicant.Addresses != null && applicant.Addresses.Count > 0)
                            application.Addresses = applicant.Addresses;
                    },
                    () => ApplicantService.Delete (application.ApplicantId)
                );
                var addApplicationCommand = new Command (
                    () => {
                        application.ApplicationNumber = applicationNumber;
                        application.ApplicationDate = currentApplication.ApplicationDate;
                        application.ExpiryDate = currentApplication.ExpiryDate;
                        Logger.Info ($"[ApplicationService] ApplicationRepository UpdateApplication method execution started for [{applicationNumber}]  Request :  [{  (application != null ? JsonConvert.SerializeObject(application) : null)}] ");
                        ApplicationRepository.UpdateApplication (applicationNumber, application);
                        Logger.Info ($"[ApplicationService] ApplicationRepository UpdateApplication method execution finished for [{applicationNumber}]");
                    },
                    () => ApplicationRepository.Remove (application)
                );
                CommandExecutor.Execute (new List<Command> { updateApplicantCommand, addApplicationCommand });

            });

            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
            Logger.Info ($"[ApplicationService] UpdateApplication method execution finished for [{applicationNumber}] result : [{  (application != null ? JsonConvert.SerializeObject(application) : null)} ");
            return application;
        }
        public async Task<IApplication> GetByApplicationNumber (string applicationNumber) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException ($"{nameof(applicationNumber)} cannot be null");

            var application = await ApplicationRepository.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} not found");

            return application;
        }

        public async Task UpdateBankInformation (string applicationNumber, IBankInformation bankInformationRequest) {
            var BankId = bankInformationRequest.BankId;
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (bankInformationRequest == null)
                throw new ArgumentNullException (nameof (bankInformationRequest));

            if (!Enum.IsDefined (typeof (AccountType), bankInformationRequest.AccountType))
                throw new InvalidEnumArgumentException (nameof (bankInformationRequest.AccountType), (int) bankInformationRequest.AccountType, typeof (AccountType));

            if (bankInformationRequest.BankAddresses != null) {
                bankInformationRequest.BankAddresses.AddressId = string.IsNullOrWhiteSpace (bankInformationRequest.BankAddresses.AddressId) ? GenerateUniqueId () : bankInformationRequest.BankAddresses.AddressId;
            }
            //bankInformationRequest.AccountType = AccountType.Savings;
            var application = await ApplicationRepository.UpdateBankInformation (applicationNumber, bankInformationRequest);
            var applicantObj = await ApplicantService.Get (application.ApplicantId);

            if (string.IsNullOrWhiteSpace (BankId) || (applicantObj.BankInformation != null && !applicantObj.BankInformation.Any (x => x.BankId == BankId))) {
                if (applicantObj.BankInformation != null) {
                    foreach (var bank in applicantObj.BankInformation) {
                        await ApplicantService.DeleteBank (application.ApplicantId, bank.BankId);
                    }
                }
                await ApplicantService.AddBank (application.ApplicantId, application.BankInformation);
            } else {
                await ApplicantService.UpdateBank (application.ApplicantId, BankId, application.BankInformation);
            }
            await EventHubClient.Publish (new ApplicationBankModified { NewBankInformation = bankInformationRequest, OldBankInformation = application.BankInformation, ApplicationNumber = applicationNumber });
            //application.BankInformation = bankInformationRequest;
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task UpdateEmailInformation (string applicationNumber, IEmailAddress emailInformationRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (emailInformationRequest == null)
                throw new ArgumentNullException (nameof (emailInformationRequest));

            var application = await ApplicationRepository.UpdateEmailInformation (applicationNumber, emailInformationRequest);
            await ApplicantService.AddEmailAddress (application.ApplicantId, application.EmailAddress);
            await EventHubClient.Publish (new ApplicationEmailModified { NewEmailAddress = emailInformationRequest, OldEmailAddress = application.EmailAddress, ApplicationNumber = applicationNumber });
            //application.EmailAddress = emailInformationRequest;
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task UpdateIncomeInformation (string applicationNumber, IIncomeInformation incomeInformationRequest) {
            EnsureInputIsValid (applicationNumber, incomeInformationRequest);
            var application = await ApplicationRepository.UpdateIncomeInformation (applicationNumber, incomeInformationRequest);
            await EventHubClient.Publish (new ApplicationIncomeModified { NewIncomeInformation = incomeInformationRequest, OldIncomeInformation = application.IncomeInformation, ApplicationNumber = applicationNumber });
            application.IncomeInformation = incomeInformationRequest;
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });

        }

        public async Task UpdateEmploymentDetail (string applicationNumber, IEmploymentDetail employmentInformationRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (employmentInformationRequest == null)
                throw new ArgumentNullException (nameof (employmentInformationRequest));

            employmentInformationRequest.EmploymentId = string.IsNullOrWhiteSpace (employmentInformationRequest.EmploymentId) ? GenerateUniqueId () : employmentInformationRequest.EmploymentId;

            if (employmentInformationRequest.Addresses != null && employmentInformationRequest.Addresses.Any ()) {
                foreach (var address in employmentInformationRequest.Addresses) {
                    address.AddressId = string.IsNullOrWhiteSpace (address.AddressId) ? GenerateUniqueId () : address.AddressId;
                }
            }

            if (employmentInformationRequest.WorkPhoneNumbers != null && employmentInformationRequest.WorkPhoneNumbers.Any ()) {
                foreach (var phoneNumber in employmentInformationRequest.WorkPhoneNumbers) {
                    phoneNumber.PhoneId = string.IsNullOrWhiteSpace (phoneNumber.PhoneId) ? GenerateUniqueId () : phoneNumber.PhoneId;
                }
            }
            var application = await ApplicationRepository.UpdateEmploymentInformation (applicationNumber, employmentInformationRequest);
            await ApplicantService.AddEmployment (application.ApplicantId, application.EmploymentDetail);
            await EventHubClient.Publish (new ApplicationEmploymentModified { NewEmploymentDetail = employmentInformationRequest, OldEmploymentDetail = application.EmploymentDetail, ApplicationNumber = applicationNumber });
            //application.EmploymentDetail = employmentInformationRequest;
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task UpdateSelfDeclaredExpense (string applicationNumber, IExpenseDetailRequest declareExpenseRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (declareExpenseRequest == null)
                throw new ArgumentNullException (nameof (declareExpenseRequest));

            var application = await ApplicationRepository.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application {applicationNumber} not found");

            ISelfDeclareExpense expense = new SelfDeclareExpense ();
            if (application.SelfDeclareExpense != null) {
                expense.CreditCardBalances = declareExpenseRequest.CreditCardBalances != null ? declareExpenseRequest.CreditCardBalances.Value : application.SelfDeclareExpense.CreditCardBalances;
                expense.DebtPayments = declareExpenseRequest.DebtPayments != null ? declareExpenseRequest.DebtPayments.Value : application.SelfDeclareExpense.DebtPayments;
                expense.MonthlyExpenses = declareExpenseRequest.MonthlyExpenses != null ? declareExpenseRequest.MonthlyExpenses.Value : application.SelfDeclareExpense.MonthlyExpenses;
                expense.MonthlyRent = declareExpenseRequest.MonthlyRent != null ? declareExpenseRequest.MonthlyRent.Value : application.SelfDeclareExpense.MonthlyRent;
            } else {
                expense.CreditCardBalances = declareExpenseRequest.CreditCardBalances.Value;
                expense.DebtPayments = declareExpenseRequest.DebtPayments.Value;
                expense.MonthlyExpenses = declareExpenseRequest.MonthlyExpenses.Value;
                expense.MonthlyRent = declareExpenseRequest.MonthlyRent.Value;
            }

            var applicationResult = await ApplicationRepository.UpdateSelfDeclaredExpense (applicationNumber, expense);
            await EventHubClient.Publish (new ApplicationSelfDeclaredExpenseModified { NewSelfDeclareExpense = expense, OldSelfDeclareExpense = application.SelfDeclareExpense, ApplicationNumber = applicationNumber });
            application.SelfDeclareExpense = expense;

            if (!string.IsNullOrWhiteSpace (declareExpenseRequest.ResidenceType)) {
                var residenceType = await ApplicationRepository.UpdateResidenceType (applicationNumber, declareExpenseRequest.ResidenceType);
                application.ResidenceType = residenceType;
            }
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task UpdateAddress (string applicationNumber, IAddress address) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (address == null)
                throw new ArgumentNullException (nameof (address));

            var application = await ApplicationRepository.UpdateAddress (applicationNumber, address);
            var updatedAddress = application.Addresses.Where (x => x.AddressId == address.AddressId).FirstOrDefault ();
            await ApplicantService.UpdateAddress (application.ApplicantId, address.AddressId, updatedAddress);
            await EventHubClient.Publish (new ApplicationAddressModified { Address = updatedAddress, ApplicationNumber = applicationNumber });
            //application.EmailAddress = emailInformationRequest;
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task UpdateLoanAmount (string applicationNumber, IUpdateLoanAmountRequest requestLoanAmount) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (requestLoanAmount == null)
                throw new ArgumentNullException (nameof (requestLoanAmount));

            if (requestLoanAmount.RequestLoanAmount <= 0)
                throw new InvalidOperationException ("RequestLoanAmount should be grater than zero");
            var application = await ApplicationRepository.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");
            var oldLoanAmount = application.RequestedAmount;
            application.RequestedAmount = requestLoanAmount.RequestLoanAmount;
            ApplicationRepository.Update (application);

            await EventHubClient.Publish (new ApplicationLoanAmountModified { OldLoanAmount = oldLoanAmount, NewLoanAmount = requestLoanAmount.RequestLoanAmount, ApplicationNumber = applicationNumber });
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }
        #endregion

        private async Task<IApplication> SaveApplication (IApplicationRequest request) {
            Application application = null;
            IApplicant applicant = null;
            await Task.Run (() => {
                //EnsureInputIsValid(request);
                application = new Application (request);

                var addApplicantCommand = new Command (
                    () => {
                        var primaryApplicant = GetApplicatRequest (request.PrimaryApplicant);
                        var applicantRequest = new ApplicantRequest (primaryApplicant);
                        applicant = ApplicantService.Add (applicantRequest).Result;
                        if (applicant == null)
                            throw new InvalidOperationException ("Applicant is not generated");
                        application.ApplicantId = applicant.Id;
                        if (applicant.BankInformation != null && applicant.BankInformation.Count > 0)
                            application.BankInformation.BankId = applicant.BankInformation[0].BankId;
                        if (applicant.EmailAddresses != null && applicant.EmailAddresses.Count > 0)
                            application.EmailAddress.Id = applicant.EmailAddresses[0].Id;
                        if (applicant.EmploymentDetails != null && applicant.EmploymentDetails.Count > 0)
                            application.EmploymentDetail.EmploymentId = applicant.EmploymentDetails[0].EmploymentId;
                        if (applicant.PhoneNumbers != null && applicant.PhoneNumbers.Count > 0)
                            application.PhoneNumbers = applicant.PhoneNumbers;
                        if (applicant.Addresses != null && applicant.Addresses.Count > 0)
                            application.Addresses = applicant.Addresses;
                    },
                    () => ApplicantService.Delete (application.ApplicantId)
                );
                var addApplicationCommand = new Command (
                    () => {
                        application.ApplicationNumber = ApplicationNumberGenerator.TakeNext ("application").Result;

                        if (string.IsNullOrWhiteSpace (application.ApplicationNumber))
                            throw new Exception ("Unable to generate application number");

                        application.ApplicationDate = new TimeBucket (TenantTime.Now); {
                            application.ExpiryDate =
                                new TimeBucket (TenantTime.Now.AddHours (ApplicationConfigurations.ExpiryDays));
                        }
                        ApplicationRepository.Add (application);
                    },
                    () => ApplicationRepository.Remove (application)
                );
                CommandExecutor.Execute (new List<Command> { addApplicantCommand, addApplicationCommand });
            });
            await EventHubClient.Publish (new ApplicationCreated { Application = application, Applicant = applicant });
            return application;
        }

        public async Task UpdateLoanPurpose (string applicationNumber, string loanPurpose, string otherLoanPurpose) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (string.IsNullOrWhiteSpace (loanPurpose))
                throw new ArgumentNullException (nameof (loanPurpose));

            var purposeOfLoanExists = LookupService.GetLookupEntry ("application-purposeOfLoan", loanPurpose);
            if (purposeOfLoanExists == null)
                throw new InvalidArgumentException ("Purpose of loan is not valid");

            var application = await ApplicationRepository.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            await ApplicationRepository.UpdateLoanPurpose (applicationNumber, loanPurpose, otherLoanPurpose);
            application.PurposeOfLoan = loanPurpose;
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }
        public async Task UpdateHunterStatus (string applicationNumber, string hunterid, bool? hunterstatus) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            var application = await ApplicationRepository.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            await ApplicationRepository.UpdateHunterStatus (applicationNumber, hunterid, hunterstatus);
            application.HunterStatus = hunterstatus;
            application.HunterId = hunterid;
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task UpdateSchemeCode (string applicationNumber, string schemecode) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            var application = await ApplicationRepository.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            await ApplicationRepository.UpdateSchemeCode (applicationNumber, schemecode);
            application.SchemeCode = schemecode;
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task UpdateDocumentCollectedSource (string applicationNumber, string documentcollectedsource) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            var application = await ApplicationRepository.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");

            await ApplicationRepository.UpdateDocumentCollectedSource (applicationNumber, documentcollectedsource);
            application.DocumentCollectedSource = documentcollectedsource;
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task<List<string>> GetAllApplicationNumber () {
            return await ApplicationRepository.GetAllApplicationNumber ();
        }

        public async Task ExtendApplicationExpiry (string applicationNumber, int byDays, bool shouldExtendExpiryDate) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (byDays <= 0)
                throw new InvalidArgumentException ("Invalid value of Extend by days. It must be a positive non-zero number.");

            await ApplicationRepository.ExtendApplicationExpiry (applicationNumber, byDays, shouldExtendExpiryDate);
            var application = await ApplicationRepository.GetByApplicationNumber (applicationNumber);
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }

        public async Task<List<IApplication>> GetApplicationsWithTrackingCode () {
            var CurrentDateTime = TenantTime.Now;
            TimeBucket dtmin = new TimeBucket (CurrentDateTime.AddHours (-24));
            TimeBucket dtmax = new TimeBucket (CurrentDateTime);

            return await ApplicationRepository.GetApplicationsWithTrackingCode (dtmin, dtmax);
        }

        #region Validations

        private void EnsureInputIsValid (IApplicationRequest applicationRequest) {
            if (applicationRequest == null)
                throw new ArgumentNullException (nameof (applicationRequest), "application cannot be empty");

            if (!Enum.IsDefined (typeof (LoanFrequency), applicationRequest.RequestedTermType))
                throw new InvalidEnumArgumentException (nameof (applicationRequest.RequestedTermType), (int) applicationRequest.RequestedTermType, typeof (LoanFrequency));

            if (!Enum.IsDefined (typeof (PaymentFrequency), applicationRequest.EmploymentDetail.IncomeInformation.PaymentFrequency))
                throw new InvalidEnumArgumentException (nameof (applicationRequest.EmploymentDetail.IncomeInformation.PaymentFrequency), (int) applicationRequest.EmploymentDetail.IncomeInformation.PaymentFrequency, typeof (PaymentFrequency));

            if (!Enum.IsDefined (typeof (SystemChannel), applicationRequest.Source.SystemChannel))
                throw new InvalidEnumArgumentException (nameof (applicationRequest.Source.SystemChannel), (int) applicationRequest.Source.SystemChannel, typeof (SystemChannel));

            if (applicationRequest.RequestedAmount <= 0)
                throw new ArgumentNullException ($"Application {nameof(applicationRequest.RequestedAmount)} is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.PurposeOfLoan))
                throw new ArgumentNullException ($" Application {nameof(applicationRequest.PurposeOfLoan)} is mandatory");

            var purposeOfLoanExists = LookupService.GetLookupEntry ("application-purposeOfLoan", applicationRequest.PurposeOfLoan);
            if (purposeOfLoanExists == null)
                throw new InvalidArgumentException ("Purpose of loan is not valid");

            var residenceTypeExists = LookupService.GetLookupEntry ("application-residenceType", applicationRequest.ResidenceType);
            if (residenceTypeExists == null)
                throw new InvalidArgumentException ("Residence type is not valid.");

            if (applicationRequest.Source == null)
                throw new ArgumentNullException ($"Application {nameof(applicationRequest.Source)} is mandatory");

            //if (string.IsNullOrWhiteSpace(applicationRequest.Source.SourceReferenceId))
            //    throw new ArgumentNullException($"Application {nameof(applicationRequest.Source.SourceReferenceId)} is mandatory");

            if (!Enum.IsDefined (typeof (SourceType), applicationRequest.Source.SourceType))
                throw new InvalidEnumArgumentException (nameof (applicationRequest.Source.SourceType), (int) applicationRequest.Source.SourceType, typeof (SourceType));

            if (!(applicationRequest.Source.SourceType == SourceType.Organic) && string.IsNullOrEmpty (applicationRequest.Source.SourceReferenceId))
                throw new ArgumentNullException ("Source reference id is mandatory");

            if (applicationRequest.PrimaryApplicant == null)
                throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant)} is mandatory");

            if (string.IsNullOrEmpty (applicationRequest.PrimaryApplicant.UserName) && string.IsNullOrEmpty (applicationRequest.PrimaryApplicant.Password)) {
                if (string.IsNullOrEmpty (applicationRequest.PrimaryApplicant.UserId))
                    throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant.UserId)} is mandatory");
            }

            if (string.IsNullOrEmpty (applicationRequest.PrimaryApplicant.UserId)) {
                if (string.IsNullOrEmpty (applicationRequest.PrimaryApplicant.UserName))
                    throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant.UserName)} is mandatory");

                if (string.IsNullOrEmpty (applicationRequest.PrimaryApplicant.Password))
                    throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant.Password)} is mandatory");
            }

            if (string.IsNullOrEmpty (applicationRequest.PrimaryApplicant.Salutation))
                throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant.Salutation)} is mandatory");

            if (string.IsNullOrEmpty (applicationRequest.PrimaryApplicant.FirstName))
                throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant.FirstName)} is mandatory");

            if (string.IsNullOrEmpty (applicationRequest.PrimaryApplicant.LastName))
                throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant.LastName)} is mandatory");

            if (applicationRequest.PrimaryApplicant.DateOfBirth == null)
                throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant.DateOfBirth)} is mandatory");

            if (!Enum.IsDefined (typeof (MaritalStatus), applicationRequest.PrimaryApplicant.MaritalStatus))
                throw new InvalidEnumArgumentException (nameof (applicationRequest.PrimaryApplicant.MaritalStatus), (int) applicationRequest.PrimaryApplicant.MaritalStatus, typeof (MaritalStatus));

            if (!Enum.IsDefined (typeof (Gender), applicationRequest.PrimaryApplicant.Gender))
                throw new InvalidEnumArgumentException (nameof (applicationRequest.PrimaryApplicant.Gender), (int) applicationRequest.PrimaryApplicant.Gender, typeof (Gender));

            if (string.IsNullOrWhiteSpace (applicationRequest.PrimaryApplicant.PermanentAccountNumber))
                throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant.PermanentAccountNumber)} is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.PrimaryApplicant.AadhaarNumber))
                throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant.AadhaarNumber)} is mandatory");

            if (applicationRequest.PrimaryApplicant.Addresses == null || !applicationRequest.PrimaryApplicant.Addresses.Any ())
                throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant.Addresses)} is mandatory");

            applicationRequest.PrimaryApplicant.Addresses.ForEach (address => {
                if (string.IsNullOrWhiteSpace (address.AddressLine1))
                    throw new ArgumentNullException ($"{nameof(address.AddressLine1)} is mandatory");

                //if (string.IsNullOrWhiteSpace(address.AddressLine2))
                //    throw new ArgumentNullException($"{nameof(address.AddressLine2)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.City))
                    throw new ArgumentNullException ($"{nameof(address.City)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.State))
                    throw new ArgumentNullException ($"{nameof(address.State)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.PinCode))
                    throw new ArgumentNullException ($"{nameof(address.PinCode)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.Country))
                    throw new ArgumentNullException ($"{nameof(address.Country)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.Location))
                    throw new ArgumentNullException ($"{nameof(address.Location)} is mandatory");

                if (!Enum.IsDefined (typeof (AddressType), address.AddressType))
                    throw new InvalidEnumArgumentException ($"{nameof(address.AddressType)} is not valid");
            });

            if (applicationRequest.PrimaryApplicant.PhoneNumbers == null || !applicationRequest.PrimaryApplicant.PhoneNumbers.Any ())
                throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant.PhoneNumbers)} is mandatory");

            applicationRequest.PrimaryApplicant.PhoneNumbers.ForEach (phoneNumber => {

                if (string.IsNullOrWhiteSpace (phoneNumber.Phone))
                    throw new ArgumentNullException ($"{nameof(phoneNumber.Phone)} is mandatory");

                if (!Enum.IsDefined (typeof (PhoneType), phoneNumber.PhoneType))
                    throw new InvalidEnumArgumentException ($"{nameof(phoneNumber.PhoneType)} is not valid");
            });

            if (applicationRequest.PrimaryApplicant.EmailAddress == null)
                throw new ArgumentNullException ($"{nameof(applicationRequest.PrimaryApplicant.EmailAddress)} is mandatory");

            if (string.IsNullOrWhiteSpace (applicationRequest.PrimaryApplicant.EmailAddress.Email))
                throw new ArgumentNullException ($"{nameof(EmailAddress.Email)} is mandatory");

            if (!Enum.IsDefined (typeof (EmailType), applicationRequest.PrimaryApplicant.EmailAddress.EmailType))
                throw new InvalidEnumArgumentException ($"{nameof(applicationRequest.PrimaryApplicant.EmailAddress.EmailType)} is not valid");

            //if (applicationRequest.SelfDeclareExpense.CreditCardBalances <= 0)
            //    throw new ArgumentNullException($"{nameof(applicationRequest.SelfDeclareExpense.CreditCardBalances)} is mandatory");

            //if (applicationRequest.SelfDeclareExpense.MonthlyExpenses <= 0)
            //    throw new ArgumentNullException($"{nameof(applicationRequest.SelfDeclareExpense.MonthlyExpenses)} is mandatory");

            //if (applicationRequest.SelfDeclareExpense.MonthlyRent <= 0)
            //    throw new ArgumentNullException($"{nameof(applicationRequest.SelfDeclareExpense.MonthlyRent)} is mandatory");

            if (applicationRequest.EmploymentDetail == null)
                throw new ArgumentNullException (nameof (applicationRequest.EmploymentDetail));

            if (string.IsNullOrWhiteSpace (applicationRequest.EmploymentDetail.Name))
                throw new ArgumentNullException ($"{nameof(applicationRequest.EmploymentDetail.Name)} is mandatory");

            if (string.IsNullOrEmpty (applicationRequest.EmploymentDetail.WorkEmail))
                throw new ArgumentNullException ($"{nameof(applicationRequest.EmploymentDetail.WorkEmail)} is mandatory");

            if (string.IsNullOrEmpty (applicationRequest.EmploymentDetail.CinNumber))
                throw new ArgumentNullException ($"{nameof(applicationRequest.EmploymentDetail.CinNumber)} is mandatory");

            if (!Enum.IsDefined (typeof (EmploymentStatus), applicationRequest.EmploymentDetail.EmploymentStatus))
                throw new InvalidEnumArgumentException ($"{nameof(applicationRequest.EmploymentDetail.EmploymentStatus)} is not valid");

            if (applicationRequest.EmploymentDetail.IncomeInformation == null || applicationRequest.EmploymentDetail.IncomeInformation.Income <= 0)
                throw new InvalidArgumentException ($"{nameof(applicationRequest.EmploymentDetail.IncomeInformation.Income)} is mandatory");
        }
        private void EnsureInputIsValid (string applicationNumber, IBankInformation bankInformationRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (bankInformationRequest == null)
                throw new ArgumentNullException (nameof (bankInformationRequest));

            if (string.IsNullOrWhiteSpace (bankInformationRequest.BankName))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.BankName)} is mandatory");

            if (string.IsNullOrWhiteSpace (bankInformationRequest.AccountHolderName))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.AccountHolderName)} is mandatory");

            if (string.IsNullOrWhiteSpace (bankInformationRequest.AccountNumber))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.AccountNumber)} is mandatory");

            if (string.IsNullOrWhiteSpace (bankInformationRequest.IfscCode))
                throw new ArgumentNullException ($"{nameof(bankInformationRequest.IfscCode)} is mandatory");

            if (!Enum.IsDefined (typeof (AccountType), bankInformationRequest.AccountType))
                throw new InvalidEnumArgumentException ($"{nameof(bankInformationRequest.AccountType)} is mandatory");

            //if (bankInformationRequest.BankAddresses == null)
            //    throw new ArgumentNullException($"{nameof(bankInformationRequest.BankAddresses)} is mandatory");
        }
        private void EnsureInputIsValid (string applicationNumber, IEmailAddress emailInformationRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (emailInformationRequest == null)
                throw new ArgumentNullException (nameof (emailInformationRequest));

            if (string.IsNullOrWhiteSpace (emailInformationRequest.Email))
                throw new ArgumentNullException ($"{nameof(emailInformationRequest.Email)} is mandatory");

            if (!Enum.IsDefined (typeof (EmailType), emailInformationRequest.EmailType))
                throw new InvalidEnumArgumentException ($"{nameof(emailInformationRequest.EmailType)} is mandatory");
        }
        private void EnsureInputIsValid (string applicationNumber, IIncomeInformation incomeInformationRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (incomeInformationRequest == null)
                throw new ArgumentNullException (nameof (incomeInformationRequest));

            if (incomeInformationRequest.Income <= 0)
                throw new ArgumentNullException ($"{nameof(incomeInformationRequest.Income)} is mandatory");

            if (!Enum.IsDefined (typeof (Applicant.PaymentFrequency), incomeInformationRequest.PaymentFrequency))
                throw new InvalidArgumentException ($"{nameof(incomeInformationRequest.PaymentFrequency)} is mandatory");
        }
        private void EnsureInputIsValid (string applicationNumber, IEmploymentDetail employmentInformationRequest) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            if (employmentInformationRequest == null)
                throw new ArgumentNullException (nameof (employmentInformationRequest));

            if (string.IsNullOrWhiteSpace (employmentInformationRequest.Name))
                throw new ArgumentNullException ($"{nameof(employmentInformationRequest.Name)} is mandatory");

            //if (string.IsNullOrWhiteSpace(employmentInformationRequest.Designation))
            //    throw new ArgumentNullException($"{nameof(employmentInformationRequest.Designation)} is mandatory");

            if (employmentInformationRequest.Addresses == null)
                throw new ArgumentNullException ($"{nameof(employmentInformationRequest.Addresses)} is mandatory");

            if (employmentInformationRequest.WorkPhoneNumbers == null)
                throw new ArgumentNullException ($"{nameof(employmentInformationRequest.WorkPhoneNumbers)} is mandatory");

            if (string.IsNullOrEmpty (employmentInformationRequest.WorkEmail))
                throw new ArgumentNullException ($"{nameof(employmentInformationRequest.WorkEmail)} is mandatory");

            //if (employmentInformationRequest.LengthOfEmploymentInMonths <= 0)
            //    throw new ArgumentNullException($"{nameof(employmentInformationRequest.LengthOfEmploymentInMonths)} is mandatory");

            if (string.IsNullOrEmpty (employmentInformationRequest.CinNumber))
                throw new ArgumentNullException ($"{nameof(employmentInformationRequest.CinNumber)} is mandatory");

            if (!Enum.IsDefined (typeof (EmploymentStatus), employmentInformationRequest.EmploymentStatus))
                throw new InvalidEnumArgumentException ($"{nameof(employmentInformationRequest.EmploymentStatus)} is not valid");

            employmentInformationRequest.Addresses.ForEach (address => {
                if (string.IsNullOrWhiteSpace (address.AddressLine1))
                    throw new ArgumentException ("AddressLine1 is mandatory for Work address");

                if (string.IsNullOrWhiteSpace (address.City))
                    throw new InvalidArgumentException ($"Work address {nameof(address.City)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.State))
                    throw new InvalidArgumentException ($"Work address {nameof(address.State)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.PinCode))
                    throw new InvalidArgumentException ($"Work address {nameof(address.PinCode)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.Country))
                    throw new InvalidArgumentException ($"Work address {nameof(address.Country)} is mandatory");

                if (!Enum.IsDefined (typeof (AddressType), address.AddressType))
                    throw new InvalidEnumArgumentException ("AddressType is not valid");
            });

            employmentInformationRequest.WorkPhoneNumbers.ForEach (phone => {
                if (string.IsNullOrWhiteSpace (phone.Phone))
                    throw new InvalidArgumentException ($"Work phone {nameof(phone.Phone)} is mandatory");

                if (!Enum.IsDefined (typeof (PhoneType), phone.PhoneType))
                    throw new InvalidEnumArgumentException ($"{nameof(phone.PhoneType)} is not valid");
            });

            if (employmentInformationRequest.IncomeInformation == null || employmentInformationRequest.IncomeInformation.Income == 0)
                throw new InvalidArgumentException ($"{nameof(employmentInformationRequest.IncomeInformation.Income)} is mandatory");
        }

        #endregion

        private static string GenerateUniqueId () {
            return Guid.NewGuid ().ToString ("N");
        }

        private ApplicantRequest GetApplicatRequest (IPrimaryApplicantDetail request) {
            var applicationrequest = new ApplicantRequest {
                EmailAddresses = new List<IEmailAddress> (),
                PhoneNumbers = new List<IPhoneNumber> (),
                Addresses = new List<IAddress> (),
                EmploymentDetails = new List<IEmploymentDetail> (),
                BankInformation = new List<IBankInformation> (),
                UserName = request.UserName,
                Password = request.Password,
                Salutation = request.Salutation,
                FirstName = request.FirstName,
                LastName = request.LastName,
                MiddleName = request.MiddleName,
                DateOfBirth = request.DateOfBirth,
                AadhaarNumber = request.AadhaarNumber,
                PermanentAccountNumber = request.PermanentAccountNumber,
                Gender = request.Gender,
                MaritalStatus = request.MaritalStatus,
                HighestEducationInformation = request.EducationInformation,
                UserId = request.UserId
            };

            if (request.EmailAddress != null)
                applicationrequest.EmailAddresses.Add (request.EmailAddress);
            if (request.PhoneNumbers != null)
                applicationrequest.PhoneNumbers.AddRange (request.PhoneNumbers);
            if (request.Addresses != null && request.Addresses.Count () > 0)
                applicationrequest.Addresses.AddRange (request.Addresses);
            if (request.EmploymentDetail != null)
                applicationrequest.EmploymentDetails.Add (request.EmploymentDetail);
            if (request.BankInformation != null)
                applicationrequest.BankInformation.Add (request.BankInformation);

            return applicationrequest;
        }
        public async Task UpdateScheduleFulfillment (string applicationNumber, IScheduleDetail scheduleDetail) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));

            var application = await ApplicationRepository.GetByApplicationNumber (applicationNumber);

            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");
            if (scheduleDetail == null)
                throw new ArgumentNullException (nameof (scheduleDetail));
            if (scheduleDetail.ScheduleDate == null)
                throw new ArgumentNullException (nameof (scheduleDetail.ScheduleDate));

            await ApplicationRepository.UpdateScheduleFulfillment (applicationNumber, new TimeBucket (scheduleDetail.ScheduleDate), scheduleDetail.PreferredAddress, scheduleDetail.ScheduleTime);
            application.ScheduleDate = new TimeBucket (scheduleDetail.ScheduleDate);
            application.PreferredAddress = scheduleDetail.PreferredAddress;
            application.ScheduleTime = scheduleDetail.ScheduleTime;
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new UpdateScheduleFulfillment { Application = application, Applicant = applicant });
        }
        public async Task UpdateLeadSquaredCRMId (string applicationNumber, string leadSquaredCRMId) {
            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new ArgumentNullException (nameof (applicationNumber));
            if (string.IsNullOrWhiteSpace (leadSquaredCRMId))
                throw new ArgumentNullException (nameof (leadSquaredCRMId));
            var application = await ApplicationRepository.GetByApplicationNumber (applicationNumber);
            if (application == null)
                throw new NotFoundException ($"Application with number {applicationNumber} is not found");
            await ApplicationRepository.UpdateLeadSquaredCRMId (applicationNumber, leadSquaredCRMId);
            var applicant = await ApplicantService.Get (application.ApplicantId);
            await EventHubClient.Publish (new ApplicationModified { Application = application, Applicant = applicant });
        }
    }
}